import React from "react";

import styles from "./style.module.scss";

interface PaginationFooterProps {
  page: number;
  pageCount: number;
  pageSize: number;
  onPageChange: (newPage: number) => void;
  onPageSizeChange: (newPageSize: number, newPage: 1) => void;
}

const PAGE_SIZES = [5, 10, 20];

const PaginationFooter: React.FC<PaginationFooterProps> = ({
  page,
  pageCount,
  pageSize,
  onPageChange,
  onPageSizeChange,
}) => {
  const pageSizesSet = Array.from(new Set([...PAGE_SIZES, pageSize]));

  const handlePageSizeChange = (
    event: React.ChangeEvent<HTMLSelectElement>,
  ) => {
    onPageSizeChange(Number(event.target.value), 1);
  };

  return (
    <div className={styles.pagination}>
      <button
        className={styles.paginationButton}
        onClick={() => onPageChange(page - 1)}
        disabled={page <= 1}
      >
        Previous
      </button>
      <span className={styles.paginationPage}>
        Page {page} of {pageCount}
      </span>
      <button
        className={styles.paginationButton}
        onClick={() => onPageChange(page + 1)}
        disabled={page >= pageCount}
      >
        Next
      </button>
      <div className={styles.paginationPageSize}>
        <label htmlFor="pageSize">Items per page:</label>
        <select
          id="pageSize"
          name="pageSize"
          value={pageSize}
          onChange={handlePageSizeChange}
        >
          {pageSizesSet.map((size) => (
            <option key={size} value={size}>
              {size}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export type { PaginationFooterProps };

export default PaginationFooter;
