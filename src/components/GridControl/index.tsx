import React from "react";

import { DataValue, SortType } from "@/shared/types";

import styles from "./style.module.scss";

interface GridControlProps<Key extends string> {
  columns: Key[];
  data: Array<Record<Key, DataValue> & Record<string, any>>;

  sortBy?: Key;
  sortType?: SortType;

  onSort?: (sortBy: Key, sortType: SortType) => void;
}

const GridControl = <Key extends string>(props: GridControlProps<Key>) => {
  const { sortType = "asc", sortBy } = props;

  const isAscending = sortType === "asc";

  const handleSort = (columnName: Key) => {
    if (sortBy === columnName) {
      props.onSort?.(columnName, isAscending ? "desc" : "asc");
    } else {
      props.onSort?.(columnName, "asc");
    }
  };

  return (
    <>
      <table className={styles.gridControl}>
        <thead>
          <tr>
            {props.columns.map((columnName, index) => (
              <th
                key={`th-${columnName as string}`}
                onClick={() => handleSort(columnName)}
              >
                <>
                  {columnName}
                  {props.sortBy === columnName ? (
                    isAscending ? (
                      <span data-sort="asc">&#x25B2;</span>
                    ) : (
                      <span data-sort="desc">&#x25BC;</span>
                    )
                  ) : null}
                </>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {props.data.map((rowData, index) => (
            <tr key={rowData.id ?? index} data-testid="grid-item">
              {props.columns.map((column, index) => (
                <td key={index}>{rowData[column]}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
      {!props.data.length && (
        <div className={styles.message} data-testid="empty-grid-message">
          no data found based on your filter criteria
        </div>
      )}
    </>
  );
};

export type { GridControlProps };

export default GridControl;
