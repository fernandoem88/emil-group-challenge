import React from "react";

import styles from "./style.module.scss";

interface InputLabelProps {
  label: string;
}

const InputLabel: React.FC<React.PropsWithChildren<InputLabelProps>> = ({
  label,
  children,
}) => {
  return (
    <label>
      <span>{label}:</span>
      <div className={styles.inputsGroup}>{children}</div>
    </label>
  );
};

export default InputLabel;
