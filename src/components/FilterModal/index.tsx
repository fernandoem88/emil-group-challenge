import React from "react";

import InputLabel from "@/components/InputLabel";

import { OPTIONS } from "./constants";
import { useFilterModalState } from "./hooks";
import styles from "./style.module.scss";
import { FilterModalProps } from "./types";

const FilterModal: React.FC<FilterModalProps> = ({
  title,
  filterData,
  onConfirm,
}) => {
  const {
    ageOperator,
    ageValue,
    country,
    hasChange,
    isOpen,
    handleAgeOperatorChange,
    handleAgeValueChange,
    handleConfirm,
    handleCountryChange,
    toggleOpen,
  } = useFilterModalState({ filterData, onConfirm });

  return (
    <>
      <button
        className={`${styles.btn} ${styles.btnTrigger}`}
        onClick={toggleOpen}
        data-testid="modal-trigger-btn"
      >
        {isOpen ? "close filter" : "open filter"}
      </button>
      <form
        className={`${styles.modalContent} ${isOpen ? styles.open : ""}`}
        onSubmit={handleConfirm}
      >
        <h3 className={styles.modalHeader}>{title}</h3>
        {isOpen && (
          <div className={styles.modalBody}>
            <InputLabel label="Country">
              <input value={country ?? ""} onChange={handleCountryChange} />
            </InputLabel>
            <br />
            <InputLabel label="Age">
              <input
                type="text"
                value={ageValue ?? ""}
                onChange={handleAgeValueChange}
              />
              <select
                value={ageOperator ?? ""}
                onChange={handleAgeOperatorChange}
              >
                <option value="">--</option>
                {Object.entries(OPTIONS).map(([optKey, optLabel]) => (
                  <option key={optKey} value={optKey}>
                    {optLabel}
                  </option>
                ))}
              </select>
            </InputLabel>
          </div>
        )}
        <div className={styles.modalFooter}>
          <button type="submit" className={styles.btn} disabled={!hasChange}>
            Confirm
          </button>
          <button onClick={toggleOpen}>Cancel</button>
        </div>
      </form>
    </>
  );
};

export default FilterModal;
