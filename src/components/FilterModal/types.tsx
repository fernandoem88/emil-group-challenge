import { Filter } from "@/shared/types";

interface FilterDataProps {
  country?: string;
  age?: { value: number; operator: Filter };
}

interface FilterModalProps {
  onConfirm: (filterData: FilterDataProps) => void;
  filterData: FilterDataProps;
  title: string;
}

export type { FilterDataProps, FilterModalProps };
