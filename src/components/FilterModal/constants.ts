import { Filter } from "@/shared/types";

export const OPTIONS: Record<Filter, string> = {
  eq: "Equal",
  gt: "Greater than",
  gte: "greater than or equal",
  lt: "Less than",
  lte: "Less than or equal",
};
