import { useState } from "react";

import { Filter } from "@/shared/types";

import { FilterDataProps, FilterModalProps } from "./types";

export const useFilterModalState = ({
  filterData,
  onConfirm,
}: Pick<FilterModalProps, "filterData" | "onConfirm">) => {
  const [isOpen, setIsOpen] = useState(false);

  const [country, setCountry] = useState(filterData.country);
  const [ageValue, setAgeValue] = useState(filterData.age?.value);
  const [ageOperator, setAgeOperator] = useState(filterData.age?.operator);

  const hasChange =
    country != filterData.country ||
    ageValue != filterData.age?.value ||
    ageOperator != filterData.age?.operator;

  const handleAgeValueChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAgeValue(+e.target.value || undefined);
  };

  const handleAgeOperatorChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setAgeOperator((e.target.value as Filter) || undefined);
  };

  const handleCountryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCountry(e.target.value || undefined);
  };

  const handleConfirm = (e: React.MouseEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (!hasChange) return;
    const filterFormData: FilterDataProps = {};

    if (ageValue && ageOperator) {
      filterFormData.age = {
        value: ageValue,
        operator: ageOperator,
      };
    }

    if (country) {
      filterFormData.country = country;
    }

    onConfirm(filterFormData);
    setIsOpen(false);
  };

  const toggleOpen = () => {
    if (isOpen) {
      setIsOpen(false);
      return;
    }
    setCountry(filterData.country);
    setAgeValue(filterData.age?.value);
    setAgeOperator(filterData.age?.operator);
    setIsOpen(true);
  };

  return {
    ageOperator,
    ageValue,
    country,
    hasChange,
    isOpen,
    handleAgeOperatorChange,
    handleAgeValueChange,
    handleConfirm,
    handleCountryChange,
    toggleOpen,
  };
};
