import { render, fireEvent } from "@testing-library/react";
import React from "react";

import FilterModal from "@/components/FilterModal";
import {
  FilterDataProps,
  FilterModalProps,
} from "@/components/FilterModal/types";

const onConfirm = jest.fn();

const filterData: FilterDataProps = {
  country: "United States",
  age: { value: 30, operator: "eq" },
};

const defaultProps: FilterModalProps = {
  onConfirm,
  filterData,
  title: "Filter data",
};

describe("FilterModal", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("opens and closes the modal when the open filter button is clicked", () => {
    const { getByTestId } = render(<FilterModal {...defaultProps} />);

    const triggerBtn = getByTestId("modal-trigger-btn");

    expect(triggerBtn.textContent).toBe("open filter");

    fireEvent.click(triggerBtn);

    expect(triggerBtn.textContent).toBe("close filter");
  });

  it("calls onConfirm with the correct data when the confirm button is clicked", () => {
    const { getByText, getByLabelText } = render(
      <FilterModal {...defaultProps} />,
    );
    fireEvent.click(getByText("open filter"));
    fireEvent.change(getByLabelText("Age:"), { target: { value: 25 } });
    fireEvent.change(getByLabelText("Country:"), {
      target: { value: "Spain" },
    });
    fireEvent.click(getByText("Confirm"));
    expect(onConfirm).toHaveBeenCalledWith({
      age: { value: 25, operator: "eq" },
      country: "Spain",
    });
  });

  it("disables the confirm button when there are no changes", () => {
    const { getByText } = render(<FilterModal {...defaultProps} />);
    fireEvent.click(getByText("open filter"));
    fireEvent.click(getByText("Confirm"));
    expect(onConfirm).not.toHaveBeenCalled();
  });
});
