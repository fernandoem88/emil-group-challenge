import "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";
import React from "react";

import GridControl, { GridControlProps } from "@/components/GridControl";

const data = [
  { id: 1, name: "John", age: 32 },
  { id: 2, name: "Jane", age: 27 },
];
const columns: (keyof (typeof data)[0])[] = ["name", "age"];

const onSort = jest.fn();
const defaultProps: GridControlProps<keyof (typeof data)[0]> = {
  columns,
  data,
  onSort,
};

describe("GridControl", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("renders correctly", () => {
    const { getAllByTestId } = render(<GridControl {...defaultProps} />);

    const rows = getAllByTestId("grid-item");
    expect(rows).toHaveLength(data.length);
  });

  it("calls onSort when a column is clicked", () => {
    const { getAllByRole, rerender } = render(
      <GridControl {...defaultProps} />,
    );
    const headers = getAllByRole("columnheader");

    fireEvent.click(headers[0]);
    expect(onSort).toHaveBeenCalledWith("name", "asc");

    rerender(<GridControl {...defaultProps} sortType="asc" sortBy="name" />);

    fireEvent.click(headers[0]);
    expect(onSort).toHaveBeenCalledWith("name", "desc");

    rerender(<GridControl {...defaultProps} sortType="desc" sortBy="name" />);

    fireEvent.click(headers[1]);
    expect(onSort).toHaveBeenCalledWith("age", "asc");
  });

  it("should display no data found based on your filter criteria mesage when data list is empty", () => {
    const { getByTestId } = render(<GridControl {...defaultProps} data={[]} />);

    expect(getByTestId("empty-grid-message")).toBeInTheDocument();
  });
});
