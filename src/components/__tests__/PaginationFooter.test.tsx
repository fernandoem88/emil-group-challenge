import "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";
import React from "react";

import PaginationFooter, {
  PaginationFooterProps,
} from "@/components/PaginationFooter";

const onPageChange = jest.fn();
const onPageSizeChange = jest.fn();

const defaultProps: PaginationFooterProps = {
  page: 2,
  pageCount: 5,
  pageSize: 10,
  onPageChange,
  onPageSizeChange,
};

describe("PaginationFooter", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("displays pagination text correctly", () => {
    const { getByText } = render(<PaginationFooter {...defaultProps} />);
    const label = getByText(
      `Page ${defaultProps.page} of ${defaultProps.pageCount}`,
    );
    expect(label).toBeInTheDocument();
  });

  it('calls onPageChange when the "Previous" button is clicked', () => {
    const { getByText } = render(<PaginationFooter {...defaultProps} />);
    const prevButton = getByText("Previous");
    fireEvent.click(prevButton);
    expect(onPageChange).toHaveBeenCalledWith(1);
  });

  it('calls onPageChange when the "Next" button is clicked', () => {
    const { getByText } = render(<PaginationFooter {...defaultProps} />);
    const nextButton = getByText("Next");
    fireEvent.click(nextButton);
    expect(onPageChange).toHaveBeenCalledWith(3);
  });

  it("calls onPageSizeChange when the page size select changes", () => {
    const { getByLabelText } = render(<PaginationFooter {...defaultProps} />);
    const pageSizeSelect = getByLabelText(
      "Items per page:",
    ) as HTMLSelectElement;
    fireEvent.change(pageSizeSelect, { target: { value: "5" } });
    expect(onPageSizeChange).toHaveBeenCalledWith(5, 1);
  });
});
