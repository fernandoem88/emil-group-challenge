import lodashSet from "lodash.set";
import { NextRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";

import { FILTERS } from "./constants";
import { DataValue, Filter, Person, SortType } from "./types";

const isSortType = (sortType: string): sortType is SortType =>
  ["asc", "desc"].includes(sortType);

const isString = (value: unknown): value is string => typeof value === "string";

const isFilter = (value: string): value is Filter =>
  FILTERS.has(value as Filter);

/**
 *
 * @description
 * compares 2 values and returns `1` if `a` is greater than `b`, `-1` if `b` is greater than `a`, `0 otherwise`.
 * @param a
 * @param b
 * @returns number
 */
const aLocaleCompareB = (a: DataValue, b: DataValue) => {
  if (a === b) return 0;

  if (isString(a) || isString(b)) return String(a).localeCompare(String(b));

  return a > b ? 1 : -1;
};

/**
 * @function
 * @param {Object} query - parsed URL query parameters
 * @returns {Object} the computed grid query properties: sortBy, sortType, pageSize, pageNumber, filter
 *
 * @description
 * This function is used to parse url query parameters object and
 * convert it into another object that can be used as input of the grid control component.
 */
const getGridQuery = <T extends string>(query: ParsedUrlQuery) => {
  const sortBy = isString(query.sortBy) ? (query.sortBy as T) : undefined;

  const sortType =
    isString(query.sortType) && isSortType(query.sortType)
      ? query.sortType
      : undefined;

  const pageSize = isString(query.pageSize)
    ? parseInt(query.pageSize, 10)
    : undefined;

  const pageNumber = isString(query.pageNumber)
    ? parseInt(query.pageNumber, 10) || 1
    : 1;

  const filterQuery: {
    filter: { country?: string; age?: { value: number; operator: Filter } };
  } = { filter: {} };

  Object.entries(query).forEach(([key, value]) => {
    if (key.indexOf("filter.") === 0 && value) {
      lodashSet(filterQuery, key, value);
    }
  });

  const { filter } = filterQuery;

  return { sortBy, sortType, pageSize, pageNumber, filter };
};

const updateUrlQuery = <Query extends Record<string, any>>(
  router: NextRouter,
  newQuery: Query,
) => {
  const noEmptyQuery = Object.entries({
    ...router.query,
    ...newQuery,
  }).reduce((acc, [key, value]) => {
    return value ? { ...acc, [key]: value } : acc;
  }, {});
  const urlSearchParams = new URLSearchParams(noEmptyQuery);
  const baseUrl = router.asPath.replace(/\?.*/g, "");
  const urlQueryString = urlSearchParams.toString();
  return `${baseUrl}?${urlQueryString}`;
};

const filterByAgeAndCountry = (
  data: Person[],
  filter: { country?: string; age?: { value: number; operator: Filter } },
) => {
  const { country, age } = filter;

  return data.filter((item) => {
    if (country && item.country !== country) return false;

    if (age) {
      switch (age.operator) {
        case "eq":
          return +item.age === +age.value;
        case "gt":
          return +item.age > +age.value;
        case "gte":
          return +item.age >= +age.value;
        case "lt":
          return +item.age < +age.value;
        case "lte":
          return +item.age <= +age.value;
      }
    }

    return true;
  });
};

export {
  aLocaleCompareB,
  getGridQuery,
  isSortType,
  isString,
  isFilter,
  updateUrlQuery,
  filterByAgeAndCountry,
};
