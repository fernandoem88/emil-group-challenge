import { isFilter, isSortType, isString } from "@/shared/helpers";

describe("helpers", () => {
  describe("isSortType", () => {
    it("should return true for 'asc'", () => {
      expect(isSortType("asc")).toBe(true);
    });

    it("should return true for 'desc'", () => {
      expect(isSortType("desc")).toBe(true);
    });

    it("should return false for 'up'", () => {
      expect(isSortType("up")).toBe(false);
    });
  });

  describe("isString", () => {
    it("should return true for a string", () => {
      expect(isString("hello")).toBe(true);
    });

    it("should return false for a number", () => {
      expect(isString(123)).toBe(false);
    });
  });

  describe("isFilter", () => {
    it("should return true for 'gt'", () => {
      expect(isFilter("gt")).toBe(true);
    });

    it("should return true for 'lt'", () => {
      expect(isFilter("lt")).toBe(true);
    });

    it("should return false for 'up'", () => {
      expect(isFilter("up")).toBe(false);
    });
  });
});
