import { NextRouter } from "next/router";

import { FilterDataProps } from "@/components/FilterModal/types";
import {
  aLocaleCompareB,
  getGridQuery,
  updateUrlQuery,
  filterByAgeAndCountry,
} from "@/shared/helpers";
import { Person } from "@/shared/types";

describe("helpers", () => {
  describe("aLocaleCompareB", () => {
    it("should return 1 if a > b", () => {
      expect(aLocaleCompareB(2, 1)).toBe(1);
    });

    it("should return -1 if a < b", () => {
      expect(aLocaleCompareB(1, 2)).toBe(-1);
    });

    it("should return 0 if a = b", () => {
      expect(aLocaleCompareB(1, 1)).toBe(0);
    });

    it("should work for strings", () => {
      expect(aLocaleCompareB("a", "b")).toBe(-1);
    });
  });

  describe("getGridQuery", () => {
    it("should return correct sortBy, sortType, pageSize, pageNumber, filter", () => {
      const query = {
        sortBy: "name",
        sortType: "asc",
        pageSize: "30",
        pageNumber: "2",
        "filter.country": "US",
        "filter.age.value": "25",
        "filter.age.operator": "gt",
      };

      const expectedOutput = {
        sortBy: "name",
        sortType: "asc",
        pageSize: 30,
        pageNumber: 2,
        filter: {
          country: "US",
          age: { value: "25", operator: "gt" },
        },
      };

      expect(getGridQuery<"name">(query)).toEqual(expectedOutput);
    });
  });

  describe("updateUrlQuery", () => {
    it("should update the URL query with new query params", () => {
      const router = {
        asPath: "/path?sortBy=name&sortType=asc&pageSize=20",
        query: {
          sortBy: "name",
          sortType: "asc",
          pageSize: "20",
        },
      } as any as NextRouter;

      const newQuery = { pageNumber: "2" };

      const newUrl = updateUrlQuery(router, newQuery);
      expect(newUrl).toBe(
        "/path?sortBy=name&sortType=asc&pageSize=20&pageNumber=2"
      );
    });
  });

  describe("filterByAgeAndCountry", () => {
    it("should filter people correctly based on filter criteria", () => {
      const data: Person[] = [
        { id: 1, name: "John", age: 30, country: "US" },
        { id: 2, name: "Jane", age: 20, country: "US" },
        { id: 3, name: "Mike", age: 35, country: "Canada" },
        { id: 4, name: "Sara", age: 25, country: "US" },
        { id: 5, name: "Chris", age: 28, country: "Canada" },
      ];

      const filter: FilterDataProps = {
        country: "US",
        age: { value: 25, operator: "gte" },
      };

      const filteredData = filterByAgeAndCountry(data, filter);

      expect(filteredData).toEqual([
        { id: 1, name: "John", age: 30, country: "US" },
        { id: 4, name: "Sara", age: 25, country: "US" },
      ]);
    });
  });
});
