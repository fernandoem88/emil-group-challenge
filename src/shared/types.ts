type Filter = "gt" | "gte" | "lt" | "lte" | "eq";

type SortType = "asc" | "desc";

type DataValue = string | number;

interface Person {
  id: number;
  age: number;
  name: string;
  country: string;
}

export type { DataValue, Filter, SortType, Person };
