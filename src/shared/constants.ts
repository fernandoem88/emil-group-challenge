import { Filter } from "./types";

const DEFAULT_PAGE_SIZE = 20;
const FILTERS = new Set<Filter>(["gt", "gte", "lt", "lte"]);

export { DEFAULT_PAGE_SIZE, FILTERS };
