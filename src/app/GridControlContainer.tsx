import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import React from "react";

import GridControl from "@/components/GridControl";
import { MOCK_PEOPLE } from "@/shared/__mocks__";
import { DEFAULT_PAGE_SIZE } from "@/shared/constants";
import {
  aLocaleCompareB,
  filterByAgeAndCountry,
  getGridQuery,
  updateUrlQuery,
} from "@/shared/helpers";
import { Person, SortType } from "@/shared/types";

type HeaderKeys = keyof Person;
const HEADER_COLUMNS: HeaderKeys[] = ["id", "name", "age", "country"];

const GridControlContainer: React.FC<{ query: ParsedUrlQuery }> = ({
  query,
}) => {
  const router = useRouter();
  const urlQuery = getGridQuery<HeaderKeys>(query ?? {});

  const isAscending = (urlQuery.sortType ?? "asc") === "asc";

  const pageSize = urlQuery.pageSize ?? DEFAULT_PAGE_SIZE;
  const pageStartINdex = (urlQuery.pageNumber - 1) * pageSize;
  const pageEndINdex = urlQuery.pageNumber * pageSize;

  const filteredData = filterByAgeAndCountry(MOCK_PEOPLE, urlQuery.filter);

  const sortedData = filteredData
    .slice(pageStartINdex, pageEndINdex)
    .sort((data1, data2) => {
      if (!urlQuery.sortBy) return 0;

      if (!(urlQuery.sortBy in data1)) return 0;

      const value1 = data1[urlQuery.sortBy];
      const value2 = data2[urlQuery.sortBy];

      if (value1 === value2) return 0;

      return isAscending
        ? aLocaleCompareB(value1, value2)
        : aLocaleCompareB(value2, value1);
    });

  const handleSort = (columnName: HeaderKeys, sortType: SortType) => {
    const newUrl = updateUrlQuery(router, { sortBy: columnName, sortType });
    router.push(newUrl);
  };

  return (
    <GridControl
      columns={HEADER_COLUMNS}
      data={sortedData}
      sortBy={urlQuery.sortBy}
      sortType={urlQuery.sortType}
      onSort={handleSort}
    />
  );
};

export default GridControlContainer;
