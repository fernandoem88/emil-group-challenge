import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import React from "react";

import PaginationFooter from "@/components/PaginationFooter";
import { MOCK_PEOPLE } from "@/shared/__mocks__";
import { DEFAULT_PAGE_SIZE } from "@/shared/constants";
import {
  filterByAgeAndCountry,
  getGridQuery,
  updateUrlQuery,
} from "@/shared/helpers";
import { Person } from "@/shared/types";

const PaginationFooterContainer: React.FC<{ query: ParsedUrlQuery }> = ({
  query,
}) => {
  const router = useRouter();
  const urlQuery = getGridQuery<keyof Person>(query ?? {});

  const pageSize = urlQuery.pageSize ?? DEFAULT_PAGE_SIZE;

  const filteredData = filterByAgeAndCountry(MOCK_PEOPLE, urlQuery.filter);
  const pageCount = Math.ceil(filteredData.length / pageSize);

  const handlePageChange = (pageNumber: number) => {
    const newUrl = updateUrlQuery(router, { pageNumber: String(pageNumber) });
    router.push(newUrl);
  };

  const handlePageSizeChange = (pageSize: number, pageNumber: 1) => {
    const newUrl = updateUrlQuery(router, {
      pageNumber: String(pageNumber),
      pageSize: String(pageSize),
    });
    router.push(newUrl);
  };

  return (
    <PaginationFooter
      page={urlQuery.pageNumber}
      pageCount={pageCount}
      pageSize={pageSize}
      onPageChange={handlePageChange}
      onPageSizeChange={handlePageSizeChange}
    />
  );
};

export default PaginationFooterContainer;
