import { ParsedUrlQuery } from "querystring";
import React from "react";

import FilterModalContainer from "./FilterContainer";
import GridControlContainer from "./GridControlContainer";
import PaginationFooterContainer from "./PaginationFooterContainer";
import styles from "./style.module.scss";

const App: React.FC<{ query: ParsedUrlQuery }> = ({ query }) => {
  return (
    <div className={styles.rootContainer}>
      <FilterModalContainer query={query} />
      <div className={styles.gridWrapper}>
        <GridControlContainer query={query} />
      </div>
      <PaginationFooterContainer query={query} />
    </div>
  );
};

export default App;
