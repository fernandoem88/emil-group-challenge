import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import React from "react";

import FilterModal from "@/components/FilterModal";
import { FilterDataProps } from "@/components/FilterModal/types";
import { getGridQuery, updateUrlQuery } from "@/shared/helpers";
import { Person } from "@/shared/types";

const FilterModalContainer: React.FC<{ query: ParsedUrlQuery }> = ({
  query,
}) => {
  const router = useRouter();
  const urlQuery = getGridQuery<keyof Person>(query ?? {});

  const handleFilterChange = ({ country, age }: FilterDataProps) => {
    const newUrl = updateUrlQuery(router, {
      pageNumber: 1,
      "filter.country": country ?? "",
      "filter.age.value": age?.value ?? "",
      "filter.age.operator": age?.operator ?? "",
    });
    router.push(newUrl);
  };

  return (
    <FilterModal
      onConfirm={handleFilterChange}
      title="filter settings"
      filterData={urlQuery.filter}
    />
  );
};

export default FilterModalContainer;
