# Frontend Challenge

## installation

To install the necessary dependencies, run the following command:

```bash
npm i
```

## components testing

run the following command to run the tests:

```bash
npm run test
```

## Dev Server

To start the development server, run the following command:

```bash
npm run dev
```

Then, open your browser and navigate to [localhost](http://localhost:3000) to view the application.

## folders specification

- the [shared folder](./src/shared/), contains some reusable **helpers**, **types** and **mocks**.
- the [components folder](./src/components/), contains small functional parts of the application.
- the [app folder](./src/app/), contains the main part of the application, responsible for managing the state.

## tests

Tests have been implemented for both [components](./src/components//__tests__/) and [helpers](./src/shared/__tests__/) to catch potential bugs and issues, ensuring the reliability of the application.

## Project Structure

The React ecosystem does not provide specific guidelines for structuring code, resulting in different conventions among teams. As a result, the structure of this project may differ from what you are accustomed to. I am open to discussing and considering improvements or decisions regarding the code structure, as it is a topic that can vary greatly among teams and projects.

The core implementation of the project can be found in the src folder. Test files are located in the corresponding folders, such as [shared](./src/shared/__tests__) and [components](./src/components/__tests__).

I chose to use **sass** for styling the components, rather than other options such as **Styled Components**, **Material UI**, or **Ant Design**.

I decided _to not use an animation library_ for the modal fade-in and fade-out effect, as I believe it is not necessary for the scope of this project.

Additionally, I chose to split the Grid control into independent parts for better control, specifically for the filter, grid data, and pagination footer components.

To ensure clear separation of concerns and maintain maintainability, the [components](./src/components/) and [containers](./src/app/) were separated, with containers handling the logic for computing the application state, and components handling user inputs and providing the necessary specifications for the containers to update the state.

To avoid potential issues, the sorting logic of the grid control was left out of the component. Considering the case that the query is sent to the backend and the data response comes out already sorted, a further sort in the Grid Control component would introduce a bug.

I also decided to use the **lodash.set** library instead of the entire **lodash** library.

The Filter component is flexible and can be tailored to the specific data structure. As there are many edge cases that cannot be generalized, I am open to discussing this further.

Finally, I decided not to use memoization as there are currently no performance issues or improvements needed.
